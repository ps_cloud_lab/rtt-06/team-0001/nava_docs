# AWS Team Nava Notes

## Afnan's Notes:
##### <p align="center"> 03/15/23 </p>

Benefits of EC2 over on premise servers in datacenters:
- No upfront cost.
- Doesn't take time to set up and secure datacenters.
- Not locked in or stuck with servers you don't need.
- YOU ONLY PAY FOR WHAT YOU USE.
- Instances are vertically scaleable, can be given more memory and cpu when needed.

Multitenancy:
Having independant tenants, or instances which are 
separate from each other but are functioning on 
the same hardware. A good example of this would be 
an apartment building, the tenants are separated 
from each other but still part of one building.

##### <p align="center"> 03/16/23 </p>

With EC2 instances you can either scale UP or scale OUT. 
Scaling up is adding more power to the instances.
While scaling out is to increase the amount of instances.

### EC2 Auto Scaling:

** Scales horizontally.

<img width="1031" alt="image" src="https://user-images.githubusercontent.com/124072294/225665102-00742a04-b2d1-431a-a921-27c8348c8c44.png">

You can also set desired and maximum capacity;
For example, you might configure the Auto Scaling group to scale out in response to increased demand, but only to a maximum of four Amazon EC2 instances.
Again, you pay for only the instances you use, when you use them. 

### Elastic Load Balancing:

Runs on a regional level, automatically and optimally filters traffic to multiple recources such as EC2 instances.

It is a single point of contact for all incoming traffic to the previously shown auto scaling group. Elastic Load Balancing and Amazon EC2 Auto Scaling are separate services, but they work together to help ensure that applications running in Amazon EC2 can provide high performance and availability. 

<img width="1460" alt="image" src="https://user-images.githubusercontent.com/124072294/225673626-5893cad0-7c3b-4457-bc21-aedc47f1f726.png">

Example of decoupled architecture:
<img width="1304" alt="image" src="https://user-images.githubusercontent.com/124072294/225679023-8242aa14-2e58-45c6-97d0-59598c0f1492.png">

### Tightly Coupled Architecture vs Loosely Coupled Architecture

A tightly coupled architecture is where applications communicate with each other directly. This can have a negative impact in which if one application has an error and cannot recieve messages the other application will also begin to see errors. (This can be considered as a monolithic application.)

A loosely coupled architecture has buffers between applications where they are isolated.
If one component fails it won't cause a cascading effect. (While this can be considered as a nicroservices approach.)

### Serverless Computing

AWS Serverless cloud computing model allows developers to build and run applications and services without worrying about server management. In a serverless architecture, the cloud provider, aws in this case, manages the infrastructure, scaling, and availability of the servers, and the developer only needs to focus on writing code.

AWS Serverless provides a range of services and tools that allow developers to create serverless applications, including AWS Lambda, Amazon API Gateway etc.

With AWS Serverless, developers can create scalable and highly available applications that can handle millions of requests per second without worrying about server provisioning, scaling, and availability. 

#### AWS Lambda vs Container Services such as ECS and EKS

AWS Lambda and container services are both serverless computing platforms provided by AWS, but they differ in how they manage and run code.

**AWS Lambda:** 
A serverless compute service that allows developers to run code without worrying about infrastructure management. It allows developers to write code in various programming languages, such as Node.js, Python, Java, Go, and more, and execute it in response to events, such as API requests, file uploads, and database changes. AWS Lambda automatically scales the compute resources to match the incoming request volume and charges only for the compute time used.

**AWS Container services suchs as ECS and EKS:**
Container services, on the other hand, are a way to run applications in containers that are packaged with all their dependencies and libraries. AWS provides container services such as Amazon Elastic Container Service (ECS) and Amazon Elastic Kubernetes Service (EKS) that allow developers to manage and run containers on a fully managed infrastructure. Container services provide more control over the environment in which the application runs, making it easier to manage dependencies and configurations.

In summary, if you need to run small, independent units of code in response to events, then Lambda is the way to go. If you need to run full-fledged applications that require more control over the environment in which they run, then container services are a better choice.

**AWS Fargate** is a serverless compute engine for containers that allows you to run Docker containers in the cloud without managing underlying infrastructure. Fargate is designed to simplify the deployment and management of containerized applications, allowing you to focus on building and running your applications rather than managing infrastructure. With Fargate, you don't need to provision or manage servers, which means you can avoid the overhead of capacity planning, patching, and maintenance.

>#### Differences between AWS Fargate, ECS and EKS:
>
>> **AWS Fargate** Fargate manages the infrastructure required to run your containers, such as virtual machines, and automatically scales the infrastructure based on your application's needs. With Fargate, you only pay for the compute resources consumed by your application.
>
>> **Amazon Elastic Container Service (ECS)** is a container management service that allows you to run Docker containers on a managed cluster of EC2 instances. You are responsible for managing the EC2 instances, but ECS automates the management of the container orchestration, load balancing, and scaling. You can run ECS on either a cluster of EC2 instances or on Fargate.
>
>> **Amazon Elastic Kubernetes Service (EKS)** is a fully managed Kubernetes service that allows you to run Kubernetes clusters on AWS without having to manage the underlying infrastructure. EKS simplifies the deployment, scaling, and management of Kubernetes applications and provides a highly available and secure environment. EKS is based on the open-source Kubernetes project and is fully compatible with Kubernetes tooling and APIs.

##### <p align="center"> 03/18/21 </p>

### AWS High Availability and Fault Tolerance.
#### AWS Regions
-  AWS builds **regions** closest to where business traffic demands.
- Locations sucs as Paris, Tokyo, Ohio
- Each reagion has multiple data centers.
- THey are connected with high speed fiber network.
- Each reagion is isolated from other regions, data doesn't flow through them unless they are explicitly told to do so.
- This is called regioal data soverignty.
- Pricing varies depending on regions.

#### AWS Availability Zones
- One or more discrete data centers. 
- Spread out in the region.

<img width="1095" alt="image" src="https://user-images.githubusercontent.com/124072294/226136387-e3b3e111-e05a-4aa5-b27b-aecd5dcfee0e.png">

#### Edge Locations
An edge location is a site that Amazon CloudFront uses to store cached copies of your content closer to your customers for faster delivery.

<img width="904" alt="image" src="https://user-images.githubusercontent.com/124072294/226137936-cc80d551-7249-4620-84f4-ff01940bf647.png">


**AWS Cloudfront**
- CDN = Content Delivery Network
- Used to cache date. Providing low latency to customers.
- Uses edge locations.


**Amazon Route 53**
- DNS = Domain Name Servie
- Used to route customers to correct web locations with low latency
- You can also buy domain names and manage them directly on AWS using Route 53.
**AWS Outposts**
- AWS Outposts is a service that allows customers to run AWS infrastructure and services on-premises or in a co-location facility. 

### API : Application programming interface
- These Api's are invoked to interact with AWS services.

This can be done with:
- AWS Management Console
- AWS Command Line Interface CLI
- AWS Software Development Kits SDKs

**AWS Elastic Beanstalk**

Platform as a Service (PaaS)

With AWS Elastic Beanstalk, you provide code and configuration settings, and Elastic Beanstalk deploys the resources necessary to perform the following tasks:
- Adjust capacity
- Load balancing
- Automatic scaling
- Application health monitoring

**AWS Cloud Formation**

Infrastructure as Code (IaC)

Allows you to define what you want in a cloud formation template in formats like JSON or YAML and AWS CloudFormation parses the document and begins to provision all the resources that were defined.

Manages all the calles to the backend APIS.


**In summary** both Elastic Beanstalk and CloudFormation help you deploy and manage applications on AWS, but they have different approaches and use cases. Elastic Beanstalk is focused on application deployment and management, while CloudFormation is focused on infrastructure provisioning and management.

### Amazon Virtual Private Cloud
### Security Groups vs Network Access Control List
<img width="1920" alt="image" src="https://user-images.githubusercontent.com/124072294/226146097-a47f430c-b0b8-4d3c-859b-0d960c505e54.png">

**Stateful** They are stateful, meaning that any inbound traffic that is allowed is automatically permitted to flow outbound, and vice versa.
Denies all inbound traffic by default.
**Stateless** meaning hey are stateless, meaning that they evaluate each incoming and outgoing packet independently based on the rules, and they can be applied to multiple subnets within a VPC.
Allows all inbound and out bound traffic by default.


**Amazon CloudFront**

Amazon CloudFront is a content delivery network (CDN) service offered by Amazon Web Services (AWS) that delivers content, videos, applications, and APIs to end-users with low latency and high transfer speeds.

CloudFront uses a global network of edge locations that cache content closer to the end-users, reducing the time it takes to load web pages and other content. When a user requests a file from a CloudFront distribution, the request is automatically routed to the nearest edge location, which then serves the file directly from the cache if it's available.

A CloudFront distribution is a collection of edge locations that cache copies of your content to improve delivery speed and reduce latency for end-users. When you create a CloudFront distribution, you specify the origin for the content that you want to deliver, which can be an S3 bucket, an EC2 instance, or a load balancer, among other options.

### Database and Storage.
**Amazon Elastic Block Storage EBS**
<img width="1012" alt="image" src="https://user-images.githubusercontent.com/124072294/226147133-cc915c83-9d34-47f3-bfeb-39af8cd39c5a.png">

<img width="1093" alt="image" src="https://user-images.githubusercontent.com/124072294/226147148-ea07eae6-54c7-401d-8ea8-6ebd45f1c98f.png">

**Amazon Elastic Block Store (EBS)** is a block storage service that provides highly available and reliable storage volumes for use with Amazon EC2 instances. Amazon EBS Snapshots are point-in-time copies of an EBS volume, which can be used for backup, disaster recovery, or to migrate data between Amazon Web Services (AWS) regions.

Needs to be in the same availabilty zone as the EC2 instances to attach to them.
Volume does not automatically scale.

An **Amazon EBS snapshot** contains all the data on the volume since the last snapshot, and not just the changes. Snapshots are incremental, meaning that only the data that has changed since the last snapshot is saved. This can help save storage space and reduce costs.

**Amazon Elastic File System EFS**

- EFS can be used to create and manage file systems that can be accessed by multiple instances simultaneously.
-  EFS supports the NFS protocol and can be accessed by instances in multiple Availability Zones within the same region. Is a regional resource.
- Linux file system.
- Automatically scales as you write to it. 
### AWS S3 Bucket

<img width="626" alt="image" src="https://user-images.githubusercontent.com/124072294/226150756-a64ab481-f90b-43b2-b35f-4917b3bd94ba.png">

<img width="1917" alt="image" src="https://user-images.githubusercontent.com/124072294/226152852-79d60b93-5e05-48ba-82da-ca06f583af2b.png">

<img width="1918" alt="image" src="https://user-images.githubusercontent.com/124072294/226152869-95b1da9e-2b09-4665-b7fc-874a427e8c51.png">


**Amazon S3 Standard**
Comes with 11 9's of durability.
Data stored in atleast 3 facilities.

**Amazon S3 Static website hosting**

**Amazon S3 Standard - Infrequent Access (S3 Standard IA)**

Good for data that requires long term storage but also quick access when needed.

**Amazon S3 Intelligent-Tiering**

Ideal for data with unknown or changing access patterns
Requires a small monthly monitoring and automation fee per object
In the Amazon S3 Intelligent-Tiering storage class, Amazon S3 monitors objects’ access patterns. If you haven’t accessed an object for 30 consecutive days, Amazon S3 automatically moves it to the infrequent access tier, Amazon S3 Standard-IA. If you access an object in the infrequent access tier, Amazon S3 automatically moves it to the frequent access tier, Amazon S3 Standard.

**Amazon S3 Glacier**
Used to archive data

**Amazon S3 Lifecycle policy** 
Can move files in between different tiers automatically depending on your configurations.

<img width="798" alt="image" src="https://user-images.githubusercontent.com/124072294/226705767-b83055f6-af52-4508-87e0-c59cca9d8bb9.png">

##### <p align="center"> 03/19/23 </p>

### Amazon Relational Database Service

**(Amazon RDS)**: 

<img width="1918" alt="image" src="https://user-images.githubusercontent.com/124072294/226208874-242c8b38-da68-4d47-af01-8823a5038016.png">

- Automated Patching
- Backups
- Redundancy
- Failover
- Disaster recovery

Data Stoerd such that it relates to other pieces of data.
Lift and shift migration 
have access to save variable as on premise.

**Amazon Aurora**
- 1/10th the price of commercial databases
- Data is replicated, you have 6 copies at any given time
- Up to 15 read replicas
- Continuous backup to Amazon S3
- Can scale up to 128 TB of storage

Consider Amazon Aurora if your workloads require high availability. It replicates six copies of your data across three Availability Zones and continuously backs up your data to Amazon S3.

**Amazon Dynamo DM**
- Serverless
- Non Relational Database / NoSQL Database

<img width="1920" alt="image" src="https://user-images.githubusercontent.com/124072294/226208905-f887d421-114f-4664-b7e8-b868e6bee7b9.png">

**Amazon Redshift**
- Data warehouse

### Data Migration

**Amazon Data Migtation Service DMS**

Migrate data securely.
Source database remains fully operational.


### AWS shared responsibility model

<img width="1918" alt="image" src="https://user-images.githubusercontent.com/124072294/226210730-fc53a4d0-85b7-4d40-89bc-8aafa048656b.png">

<img width="1916" alt="image" src="https://user-images.githubusercontent.com/124072294/226214960-d8c72a58-b737-4677-809c-e2eb7531ee8e.png">

<img width="905" alt="image" src="https://user-images.githubusercontent.com/124072294/226215025-0d9b7f26-4d00-423c-b158-9c2d06aa1837.png">


### IAM

Example of a IAM Policy:
<img width="1245" alt="image" src="https://user-images.githubusercontent.com/124072294/226215244-a22807d1-70f9-4a77-854c-8f6adce4d6b4.png">

- Effect is always either Allow or Deny
- Action can be any API call
- Resource is which specific AWS resource that API call is for

##### <p align="center"> 03/20/23 </p>

**AWS Key Management Service AWS KMS**

- enables you to perform encryption operations through the use of cryptographic keys.

**AWS Artifact**
Security and Compliace related service.
**AWS Sheild**

**Amazon Inspector**
Amazon Inspector helps to improve the security and compliance of applications by running automated security assessments. It checks applications for security vulnerabilities and deviations from security best practices, such as open access to Amazon EC2 instances and installations of vulnerable software versions. 

After Amazon Inspector has performed an assessment, it provides you with a list of security findings. The list prioritizes by severity level, including a detailed description of each security issue and a recommendation for how to fix it. However, AWS does not guarantee that following the provided recommendations resolves every potential security issue. Under the shared responsibility model, customers are responsible for the security of their applications, processes, and tools that run on AWS services.

**Amazon GuardDuty**

<img width="994" alt="image" src="https://user-images.githubusercontent.com/124072294/226431921-7f4fa312-ce9a-443d-b8ea-89067530f95d.png">

After you have enabled GuardDuty for your AWS account, GuardDuty begins monitoring your network and account activity. You do not have to deploy or manage any additional security software. GuardDuty then continuously analyzes data from multiple AWS sources, including VPC Flow Logs and DNS logs. 

If GuardDuty detects any threats, you can review detailed findings about them from the AWS Management Console. Findings include recommended steps for remediation. You can also configure AWS Lambda functions to take remediation steps automatically in response to GuardDuty’s security findings.

### Monitoring

Observing systems, collecting metrics, evaluating data to make decisons or take actions is called monitoring.

**Amazon CloudWatch**

Amazon CloudWatch is a web service that enables you to monitor and manage various metrics and configure alarm actions based on data from those metrics.

CloudWatch uses metrics to represent the data points for your resources. AWS services send metrics to CloudWatch. CloudWatch then uses these metrics to create graphs automatically that show how performance has changed over time. 

>CloudWatch alarms
>
>With CloudWatch, you can create alarms that automatically perform actions if the value of your metric has gone above or below a predefined threshold. 
>
>For example, suppose that your company’s developers use Amazon EC2 instances for application development or testing purposes. If the developers occasionally forget to stop the instances, the instances will continue to run and incur charges. 
>
>In this scenario, you could create a CloudWatch alarm that automatically stops an Amazon EC2 instance when the CPU utilization percentage has remained below a certain threshold for a specified period. When configuring the alarm, you can specify to receive a notification whenever this alarm is triggered.


**AWS CloudTrail**

The comprehensive API auditing tool. The engine is simple, every request made to AWS, it doesn't matter if it's to launch an EC2 instance, or add a row to a DynamoDB table, or change a user's permissions. Every request gets logged in the CloudTrail engine. The engine records exactly who made the request, which operator, when did they send the API call? Where were they? What was their IP address? And what was the response? Did something change? And what is the new state? Was the request denied? 


**AWS Trusted Advison**

AWS Trusted Advisor is a web service that inspects your AWS environment and provides real-time recommendations in accordance with AWS best practices.

Trusted Advisor compares its findings to AWS best practices in five categories: 

1 cost optimization 
2 performance
3 security
4 fault tolerance
5 service limits 

For the checks in each category, Trusted Advisor offers a list of recommended actions and additional resources to learn more about AWS best practices. 


**AWS Snowmobile** is a data transfer service that uses a secure, ruggedized shipping container to transfer up to 100PB of data between on-premises data centers and AWS. It is typically used when organizations need to transfer large amounts of data over long distances, and when network-based transfers are not feasible due to bandwidth limitations, security concerns, or other factors.

**Snowball** is a smaller, portable data transfer device that can be used to transfer up to 80TB of data between on-premises data centers and AWS. It is designed to be secure, with encryption used to protect data both in transit and at rest, and is typically used when organizations need to transfer large amounts of data more quickly than they could over a network connection, or when they need to transfer data to or from locations with limited or no network connectivity.

### AWS Free Tier

Three types of offers are available: 

- Always Free

For example AWS Lambda 1 million free invocation per month, and it doesn't expire.

- 12 Months Free

Amazon S3 free for 12 months for up to 5GB of storage.

- Trials

AWS Lightsail offers 1 month trial of up to 750 hours of usage.

These only scratch the surface.

**AWS Consolidated Billing**

**Free Feature

At the end of every month, instead of having to pay an AWS bill for every single account, you can roll those bills up into one bill owned by the owner of the organization. This makes it easier to keep track of your bills. You don't get 100 bills, if you have 100 AWS accounts.

There are other benefits of using this feature too. One of them is that the usage for AWS resources is rolled up to the organization level. AWS does offer bulk pricing. Each individual account may only have a small amount of usage, but you can get the bulk discount pricing because of the aggregate across all accounts in the organization. 

In addition, if you have a savings plan in place, or if you are using reserved instances for EC2, it can be shared across AWS accounts in the organization.

**AWS Budgets**

In AWS Budgets, you can create budgets to plan your service usage, service costs, and instance reservations.

The information in AWS Budgets updates three times a day. This helps you to accurately determine how close your usage is to your budgeted amounts or to the AWS Free Tier limits.

In AWS Budgets, you can also set custom alerts when your usage exceeds (or is forecasted to exceed) the budgeted amount.

<img width="494" alt="image" src="https://user-images.githubusercontent.com/124072294/226722422-c3de2243-1d2b-4f7c-9699-a9390db99153.png">


### AWS Support

**Basic Support** is free for all AWS customers. It includes access to whitepapers, documentation, and support communities. With Basic Support, you can also contact AWS for billing questions and service limit increases.

With Basic Support, you have access to a limited selection of AWS Trusted Advisor checks. Additionally, you can use the AWS Personal Health Dashboard, a tool that provides alerts and remediation guidance when AWS is experiencing events that may affect you. 

<img width="1805" alt="image" src="https://user-images.githubusercontent.com/124072294/226716727-ee96577f-e586-467f-98df-8c6dae9c530f.png">

### The AWS Well-Architected Framework

The AWS Well-Architected Framework helps you understand how to design and operate reliable, secure, efficient, and cost-effective systems in the AWS Cloud. It provides a way for you to consistently measure your architecture against best practices and design principles and identify areas for improvement.

<img width="805" alt="image" src="https://user-images.githubusercontent.com/124072294/226758922-6d92a9eb-5a91-4a1e-ae5a-11d8c20b794f.png">

Look into Cloud adoption framework
Look further into AWS Well-Architected Framework

## Angela's Notes:




## Vipra's Notes:
3/15/2023
# Amazon CloudWatch
Amazon CloudWatch monitors your Amazon Web Services (AWS) resources and the applications you run on AWS in real time.
The CloudWatch home page automatically displays metrics about every AWS service you use. 
You can additionally create custom dashboards to display metrics about your custom applications, and display custom collections of metrics that you choose.
![cloudwatch](/Images/amazon-cloudwatch.png)


## AWS Cloudtrail
Actions taken by a user or an AWS service are recorded as events in CloudTrail.
Events include actions taken in the AWS Management Console, AWS Command Line Interface, and AWS SDKs and APIs.
CloudTrail is enabled on your AWS account when you create it.
You can filter events by specifying the time and date that an API call occurred, the user who requested the action, the type of resource that was involved in the API call, and more.
# AWS Trusted Adviser
Trusted Advisor inspects your AWS environment, and then makes recommendations when opportunities exist to save money, improve system availability and performance, or help close security gaps.

Trusted Advisor compares its findings to AWS best practices in five categories: cost optimization, performance, security, fault tolerance, and service limits. For the checks in each category, Trusted Advisor offers a list of recommended actions and additional resources to learn more about AWS best practices. 

Trusted Advisor provides a suite of features so that you can customize recommendations and proactively monitor your Amazon Web Services (AWS) resources:
• Trusted Advisor notifications – Stay up to date with your AWS resource deployment. You will receive a weekly notification email message when you opt in for this service.
• Access management – Control access to specific checks or check categories. • AWS Support application programming interface (API) – Retrieve and refresh Trusted Advisor results programmatically.
• Action links – Access items in a Trusted Advisor report from hyperlinks that take you directly to the console. From the console, you can implement the Trusted Advisor recommendations.
• Recent changes – Track recent changes of check status on the console dashboard. The most recent changes appear at the top of the list to bring them to your attention.
• Exclude items – Customize the Trusted Advisor report. You can exclude items from the check result if they are not relevant.
• Refresh all – Refresh individual checks or refresh all the checks at once by choosing Refresh All in the upper-right corner of the summary dashboard. A check is eligible for 5-Minute Refresh after it was last refreshed.










3/15/2023
# AWS Cloud Adoption Framework
AWS Cloud Adoption Framework (AWS CAF) is a structured method to help organizations effectively migrate to AWS.
 **six perspectives of AWS Cloud Adoption Framework**
  - Business Perspective
  - People Perspective
  - Governance Perspective
  - Platform Perspective
  - Security Perspective
  - Operations Perspective

  ### Migration strategies
When migrating applications to the cloud, six of the most common migration strategies that can implement are:

- Rehosting
- Replatforming
- Refactoring/re-architecting
- Repurchasing
- Retaining
- Retiring

## AWS Snow Family
The AWS Snow Family is a collection of physical devices that help to physically transport up to exabytes of data into and out of AWS. 

AWS Snow Family is composed of AWS Snowcone, AWS Snowball, and AWS Snowmobile. 

![snow family](/Images/AWS%20snow%20family.jpg)

#### AWS architecure review tool 
It provides customers and partners with a consistent approach to reviewing their architectures against current AWS best practices, and gives advice on how to architecy workloads for the cloud

#### AWS Well -Architected Tool Features
- Define workloads and perform reviews
- Locate helpful resources
- Save milestones
- Use dashboards
-  Generate PDF reports
-  Assign priorities to pillars
- Create Improvement plan

#### Amazon Storage services
This is offering from AWS to store any kind of objects. You can read,write,delete and update the objet.
#### Amazon S3 Bucket
- This is most commonly used storage service of AWS
- Objects are stored in storage bucket
- size of object can be upto 5TB
- Charges will be according to data size and transfer
#### Amazon S3 storage classes
![S3](Images/AWS-S3-Storage-Classes-Featured.webp)
#### Amazon S3 standard
Amazon S3 standard designed to provide high durability,high availability and high performance object storage for frequent access data.
#### Amazon S3 Intelligent Tiering
This class is designed to minimize costs to the most cost effective access tier without impacting performances and usage. 
Amazon dont charge for moving objects between tiers.
#### Amazon S3 Standard - Infrequent Access
Infrequent Access storage is used for less frequently accessed data, but requires rapid access when needed.
#### Amazon S3 one zone - Infrequent Access
This class is similar to Infrequent Access  class but the main difference is in all other storage classes data stores  in 3 availability zone, in this class data is stored in only single availability zone.
#### Amazon Simple Storage Service Glacier
This storage class is one of the lowest cost offering of AWS.
#### Amazon S3 Glacier Deep Archive
It is lowest cost storage class of Amazon S3. It is used
 for very rarely used data.
#### Amazon Elastic Block Storage ( EBS )
This storage service is used for storing any kind of data. It is attached with EC2 instance
#### Amazon Elastic File System ( EFS )
It is used for network sharing data between different EC2 instances. It is similar to sharedrive.
#### Amazon FSx for Windows File Server
It is networking sharing data between windows operating system
#### Amazon EC2 Instance Store
It is used for store data temporarily
      

3/21/2013
#### Amazon Route 53
Amazon Route 53 is a highly available and scalable Domain Name System (DNS) web service. 
Three main functions of Amazon Route 53
- domain registration
- DNS routing 
- health checking

1.Register domain names
Your website needs a name, such as example.com . Route 53 lets you register a name for your website or web application, known as a domain name.

2.Route internet traffic to the resources for your domain
When a user opens a web browser and enters your domain name (example.com) or subdomain name (acme.example.com) in the address bar, Route 53 helps connect the browser with your website or web application.

![Route 53](Images/Route%2053.png)


3.Check the health of your resources
Route 53 sends automated requests over the internet to a resource, such as a web server, to verify that it's reachable, available, and functional. You also can choose to receive notifications when a resource becomes unavailable and choose to route internet traffic away from unhealthy resources.

#### AWS Config
AWS Config is a service used for assessing, auditing, and evaluating the configuration of your AWS resources. It continuously monitors and records your AWS resource configurations, and you can use it to automate the evaluation of recorded configurations against desired configurations.
With AWS Config, you can discover existing AWS resources and determine how a resource was configured at any point. It also provides configuration change notifications to facilitate security and governance.

You can use AWS Config together with AWS CloudTrail to gain complete visibility into the details of a configuration change. AWS Config notifies you when the configuration of a resource has changed, and CloudTrail provides you with additional details, such as who made the change.

![aws config](Images/aws%20config.webp)

#### Amazon GuardDuty
GuardDuty is a threat detection service that continuously monitors your AWS accounts and workloads for malicious activity. It delivers detailed security findings for visibility and remediation.

When you activate GuardDuty and configure it to monitor your account, GuardDuty automatically detects threats by using anomaly detection and machine learning techniques. You can view the security findings that GuardDuty produces in the GuardDuty console or through Amazon CloudWatch Events.

GuardDuty detects unauthorized and unexpected activity in your AWS environment by analyzing and processing data from different AWS service logs. These logs include the following:
• AWS CloudTrail event logs 
• Virtual private cloud (VPC) flow logs 
• Domain Name System (DNS) logs


![Guardduty](Images/20_Amazon_Guardduty_1_7b6ad99106.jpg)

#### What is Malware?
Short for malicious software, Malware is a collective term of viruses, worms, Trojans, etc. Hackers may also use some other harmful software to get into your system to destroy and collect sensitive information. Hackers intentionally build Malware to cause damage to computers and networks. They leverage victims for financial gain. They have the upper hand in pulling out all your personal information, from health and medical records to personal emails and passwords. Malware is usually distributed by email as a link or file, requiring the recipient to click on the link or open the file to execute it.

Malware can spread by a link or executable file may deliver some strains via email. Others are sent via social media or instant messaging. Even mobile phones get attacked.

- What could happen when you click those suspicious links?
The malware can get downloaded and auto-replicated in various areas of the file system.
Installing programs that record keystrokes or commander device services, often without the user’s knowledge, thus significantly slowing down the system.
Blocking connections to files, applications, or sometimes the whole machine requires the user to pay a fee to restore access.
Ads are scattered around a browser or a computer’s desktop.
Breaking critical machine components and leaving a computer useless.

#### 7 Types of Malware

1. Virus
Most common Malware binds their deceptive code to clean code and waits for an unwitting user or an automatic method to run it. They will spread rapidly and broadly, almost like a biological virus, causing harm to systems’ key functions, corrupting data, and freezing users out of their devices. Typically, they are embedded inside a binary format.

2. Worms
Worms are called Malware because of the way they infect networks. They weave their way around the network, beginning from one infected computer and linking to subsequent devices. This kind of Malware can infiltrate whole networks of computers rapidly.

3. Spyware
Spyware is embedded software that collects and transmits user personal information or web browsing patterns and data, ideally without your prior knowledge. It allows people to spy on the victim’s family, children, and colleagues. They can track all modes of contact on a specific instrument. Spyware is sometimes used to test and control communications in a confidential situation or during an investigation by law enforcement authorities, government departments, and information security organizations.

4. Adware
You might have noticed an unwanted ad bombarded at your screen for no reason, then it is Adware. Adware applications bombard consumers with unwelcome ads, which usually appear as flickering advertisements or popup windows. Adwares usually get installed with other software or on visiting spammy websites.

5. Ransomware
Ransomware is a destructive malware that lets hackers get access to personal information and encrypt that so that users can not access it and then exchange data release demands monetary payment. It is usually part of a hoax of phishing. The user installs the ransomware by clicking on suspicious links or email attachments. The attacker then encrypts such data that only a cryptographic key he knows can unlock. The data is unlocked until the intruder accepts payment.

6. Trojan virus
Trojan horses, or Trojans, are computer viruses that spread through social manipulation. A Trojan persuades unsuspecting users to mount itself by imitating something else. Another Trojan trick is to write auto-installing viruses into a USB memory stick and then hand it over to an unwitting user.

7. Fileless Malware
The virus that uses authorized programs to infect a device is known as Fileless Malware. There are no virus archives to search and no malicious operations to find in Fileless malware registry attacks. It is difficult to locate and delete since it does not depend on files and leaves no trace.

![Malware](Images/malware.jpg)



- How Can I Protect Myself from Malware?
Keep your operating system and applications updated to the latest versions. Cybercriminals search for flaws in old or obsolete applications and use those flaws to get access to your device. So make sure you install updates as soon as they’re available.
Keep the number of applications on your smartphones to a minimum. Download just the ones you believe you’ll need daily. Often, delete any apps you are no longer using.
Avoid clicking on unfamiliar links. If a link is unknown, whether it arrives via email, a social networking site, or a text message, avoid it.
Emails demanding personal information must be avoided. Do not open a link in an email that seems to come from your bank and instructs you to reset your password or enter your account. Check in to your online banking account right away.
If you are not a pro user familiar with this malware, consider installing a good and paid anti-malware program.



















## Namita's Notes:

##### <p align="center"> 03/15/23 </p>
#### SkillBuilder learning:

##### Consolidated Billing:
The consolidated billing feature of AWS Organizations enables you to receive a single bill for all AWS accounts in your organization.

By consolidating, you can easily track the combined costs of all the linked accounts in your organization. 

The default maximum number of accounts allowed for an organization is 4

Benefit of consolidated billing is the ability to share bulk discount pricing, Savings Plans, and Reserved Instances across the accounts in your organization. 

For instance, one account might not have enough monthly usage to qualify for discount pricing however, when multiple accounts are combined, their aggregated usage may result in a benefit that applies across all accounts in the organization.

#### AWS Budgets

In AWS Budgets, you can create budgets to plan your service usage, service costs, and instance reservations

The information in AWS Budgets updates three times a day.
In AWS Budgets, you can also set custom alerts when your usage exceeds (or is forecasted to exceed) the budgeted amount.
Example- Suppose you have set the budget for Amazon EC2 instance and you want to make sure that it won't exceeds $200 monthly.
![Budget](/Images/Budget.jpg)

So, in AWS you can set the custom budget to notify when it your usage has reach the half of the amount. You can also create alerts in this setting letting you to decide how you want to proceed with your continued use of Amazon Ec2 instance.gets, you can create budgets to plan your service usage, service costs, and instance reservations.



#### AWS Cost Explorer 
It is a tool that enables you to visualize, understand, and manage your AWS costs and usage over time.

AWS Cost Explorer includes a default report of the costs and usage for your top five cost-accruing AWS services. You can apply custom filters and groups to analyze your data. For example, you can view resource usage at the hourly level.

![AWS Cost Explorer](/Images/CostExplorer.jpg)

This example of the AWS Cost Explorer dashboard displays monthly costs for Amazon EC2 instances over a 6-month period. The bar for each month separates the costs for different Amazon EC2 instance types (such as t2.micro or m3.large). 

##### <p align="center"> 03/16/23 </p>

#### 6 strategies for migration

When migrating applications to the cloud, six of the most common migration strategies that you can implement are:

#### 1. Rehosting
Rehosting also known as “lift-and-shift” involves moving applications without changes. 

#### 2. Replatforming
Replatforming, also known as “lift, tinker, and shift,” involves making a few cloud optimizations to realize a tangible benefit.

#### 3. Refactoring/re-architecting
Refactoring (also known as re-architecting) involves reimagining how an application is architected and developed by using cloud-native features.

#### 4. Repurchasing
Repurchasing involves moving from a traditional license to a software-as-a-service model. 

#### 5. Retaining
Retaining consists of keeping applications that are critical for the business in the source environment. 

#### 6.Retiring
Retiring is the process of removing applications that are no longer needed

### The Well-Architected Framework is based on six pillars: 

1.  Operational excellence
2.  Security
3.  Reliability
4.  Performance efficiency
5.  Cost optimization
6.  Sustainability


#### Advantages of cloud computing

Operating in the AWS Cloud offers many benefits over computing in on-premises or hybrid environments. 

six advantages of cloud computing:

1.  Trade upfront expense for variable expense.
2.  Benefit from massive economies of scale.
3.  Stop guessing capacity.
4.  Increase speed and agility.
5.  Stop spending money running and maintaining data centers.
6.  Go global in minutes.



##### <p align="center"> 03/20/21 </p>
##### <p align="center">Security Topics
#### Security Prevention Measures

![sec_pre_measures](/Images/sec_pre_measures.jpg)

#### 1. Network Hardening Measures
Implement controls to stop threats at the network level.
1.   Network discovery hardening – Block network exploration protocols.
 – Close unused ports. 
 – Maintain an accurate and up-to-date asset inventory that identifies the list of the devices that are allowed on your network.
2.  Network security architecture hardening
    – Use firewalls.
    – Use an intrusion prevention system (IPS).
    – Segment your network

   #####  2. Systems Hardening Measures
Implement controls to stop threats at the host level.

Hosts include workstations, servers, or other devices that run services and applications or store data.
1.  Apply operating system (OS) patches and security updates regularly.
2.  Remove unused applications and services.
3.  Monitor and control configuration change

#### 3. Data security controls Implement controls to protect the data.
Example measures: 
1.   Encrypt data in transit and data at rest as needed.
2. Use digital certificates to protect information.
3.   Use data integrity checking tools. • Use role-based access control.

#### 4. Identity Management
Implement controls for user authentication and authorization.
1.  Use the principle of least privilege to control access to resources.
Information
2.   Set up a policy that enforces password strength and password expiration.
3.   Use the principles of authentication, authorization, and accounting (AAA).


##### <p align="center"> 03/21/2023 </p>
#### AWS CloudTrail

##### What is CloudTrail?
CloudTrail is an auditing, compliance monitoring, and governance tool from AWS. It is classified as a Management and Governance tool in the AWS Management Console

CloudTrail logs, continuously monitors, and retains account activity related to actions across your AWS infrastructure, which gives you control over storage, analysis, and remediation actions

![Auditlog](	https://uploads-ssl.webflow.com/5f5097f276b52f2a32f9c27a/6332f9ed7725544dac852b36_aws-cloudtrail.png)


#### Benefits of CloudTrail
There are many benefits of CloudTrail
1.   It increases your visibility into user and resource activity. With this visibility, you can identify who did what and when in your AWS account.

2.  Because CloudTrail logs activities, you can search through log data, identify actions that are noncompliant, accelerate investigations into incidents, and then expedite a response.

3.  Because you are able to capture a comprehensive history of changes that are made in your account, you can analyze and troubleshoot operational issues in your account.

4.  CloudTrail helps discover changes made to an AWS account that have the potential of putting the data or the account at heightened security risk. At the same time, it expedites AWS audit request fulfillment. This action helps to simplify auditing requirements, troubleshooting, and compliance.
 
![Test_image](https://lh3.googleusercontent.com/Lnsw4CDOM3hWRD3bCdasqnro77ehklMn-g6_m5ptn0bmOAfFth9tRJjtfZFtz0ZXheefJcZN-MHgEkZzh4Y8N41os81TjWte51exdvv6tasD-9ehIsHExNN5wQxob9evZVrVoxET)

#### CloudTrail best practices
1.  Turn on CloudTrail log file integrity validation. 
2.   Aggregate log files to a single S3 bucket.
3.  Ensure that CloudTrail is enabled across AWS globally.
4.   Restrict access to CloudTrail S3 buckets.
5.  Integrate with Amazon CloudWatch.

#### AWS Config
AWS Config is a service used for assessing, auditing, and evaluating the configuration of your AWS resources.

1. Provides AWS resource inventory, configuration history, and configuration change notifications
 
2. Provides details on all configuration changes

3. Can be used with AWS CloudTrail to gain additional details on a configuration change

4. It useful for the following:
*     Compliance auditing
*     Security analysis
*     Resource change tracking
*     Troubleshooting

#### AWS Config Capabilities
![config_cap](./Images/awsconfig_capabilities.jpg)


##### <p align="center"> 03/22/2023 </p>

#### AWS Trusted Advisor
######Trusted Advisor is an online resource to help you reduce cost, increase performance, and improve security by optimizing your AWS environment.

![ima](./Images/trustedadvisor.png)
 
1. Optimization – Save money on AWS by reducing unused and idle resources or making commitments to reserved capacity.

2. Performance – Improve the performance of your service by checking your service limits, ensuring that you take advantage of provisioned throughput, and monitoring for overutilized instances.

3. Security – Improve the security of your application by closing gaps, activating various AWS security features, and examining your permissions.

4. Fault Tolerance – Increase the availability and redundancy of your AWS application by taking advantage of automatic scaling, health checks, multiple Availability Zones, and backup capabilities.

5. Service Limits – Check for service usage that is more than 80 percent of the service limit.
The status of the check



##### Trusted Advisor features
Trusted Advisor provides a suite of features for you to customize recommendations and to proactively monitor your Amazon Web Services (AWS) resources

![image](./Images/trustedad1.jpg)

* Trusted Advisor notifications
 -Stay up to date with your AWS resource deployment. You will receive a weekly notification email message when you opt in for this service.

* Access management 
– Control access to specific checks or check categories. 
*  AWS Support application programming interface (API) 
– Retrieve and refresh Trusted Advisor results programmatically.

* Action links 
– Access items in a Trusted Advisor report from hyperlinks that take you directly to the console. From the console, you can implement the Trusted Advisor recommendations.

*  Recent changes
– Track recent changes of check status on the console dashboard. The most recent changes appear at the top of the list to bring them to your attention.

* Exclude items 
– Customize the Trusted Advisor report. You can exclude items from the check result if they are not relevant.

* Refresh all
 -Refresh individual checks or refresh all the checks at once by choosing Refresh All in the upper-right corner of the summary dashboard. A check is eligible for 5-Minute Refresh after it was last refresh

##### Trusted Advisor security checks
Trusted advisor provides the following security checks to all customers at no cost: 
1. AWS Identity and Access Management (IAM) use
2. Multi-factor authentication (MFA) on root account 
3. Security groups – Specific ports unrestricted
4. Amazon Simple Storage Service (Amazon S3) bucket permissions
 5. Amazon Elastic Block Store (Amazon EBS) public snapshots 
 6. Amazon Relational Database Service (Amazon RDS) public snapshots
 
 ###### Examples of Trusted Advisor security checks and advice include the following: 
*  Making sure that security groups do not keep ports open with unrestricted access
*  Checking for your use of IAM permissions to control access to AWS resources
*  Checking the root account and warning if MFA is not activated
*  Checking that S3 buckets do not have open access permissions



